﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Funciones
{
	class Program
	{
		//Funciones sin tipo (void)
		//y sin parametros o argumentos


		public static void Creditos() // Funcion vacia, no retorna nada. Se la puede usar en una linea nueva de codigo. Estas no tienen ni tipo ni parametros (VOID)
		{
			Console.Clear();

			Console.WriteLine("UTN - FRI");
			Console.WriteLine("PROGRAMACION I");
			Console.WriteLine("NICOLAS AYBAR CRITTO");
			Console.WriteLine("COMISION 2");
			Console.WriteLine("2018");

			Console.ReadKey();
		}


		public static bool EsPar(int _numero) // Funcion para saber si el numero es impar de tipo booleano
		{
			bool _resultado = false; // Booleano 

			if (_numero % 2 == 0) // condicion para saber si es PAR
			{
				_resultado = true;
			}

			else
			{
				_resultado = false;
			}

			return _resultado; // Tiene que retonar algo, porque es una funcion de retorno la que se plantea
		}

		public static bool EsPerfecto(int _numero) // Funcion para saber si el numero ingresado es Perfecto de tipo booleano
		{
			bool _resultado = false; // Variable bool
			int _acumulador = 0; // acumula los que son divisibles en 0

			for (int _i = 1; _i < _numero; _i++)
			{
				if (_numero % _i == 0)
				{
					_acumulador += _i;
				}
			}
			if (_acumulador == _numero)
			{
				_resultado = true;
			}


			return _resultado; // Retorna el resultado
		}

		public static bool EsPrimo(int _numero) // funcion para el numero primo tipo booleano
		{
			bool _resultado = false;
			int _acumulador = 0;


			for (int _i = 1; _i <= _numero; _i++)
			{

				if (_numero % _i == 0)
				{
					_acumulador++;

				}

			}

			if (_acumulador == 2)
			{
				_resultado = true;
			}


			return _resultado;
		}


		static void Main(string[] args) // Principal cuerpo del programa
		{
			// Declaracion variables

			int _numero = 0;
			int _contadorPar = 0;
			int _contadorPerfecto = 0;
			int _contadorPrimo = 0;
			bool _verificarDato = false;
			bool _finalizarProg = false;
			ConsoleKeyInfo _tecla;
			Console.Clear();


			// OPERAR
			// Key Info para seguir avanzando

			while (_finalizarProg == false)
			{
				_verificarDato = false;
				while (_verificarDato == false)
				{
					Console.Write("Ingrese un numero: ");
					{
						if (int.TryParse(Console.ReadLine(), out _numero))
						{

							if (EsPar(_numero)) //pongo el metodo dentro del if, porque el metodo es bool, y el if recibe bool
							{
								_contadorPar++;
							}
							if (EsPerfecto(_numero))
							{
								_contadorPerfecto++;
							}
							if (EsPrimo(_numero))
							{
								_contadorPrimo++;
							}
							_verificarDato = true;
						}
						else
						{
							Console.WriteLine("Ocurrio un error. Por favor ingrese un numero");
						}
					}
				}
				_verificarDato = false;
				while (_verificarDato == false)
				{
					Console.Write("Desea agregar otro numero? [S para SI - N para NO]: ");
					_tecla = Console.ReadKey();

					Console.WriteLine();


					if (_tecla.Key == ConsoleKey.S)
					{
						_verificarDato = true;
						_finalizarProg = false;
					}
					else if (_tecla.Key == ConsoleKey.N)
					{
						_verificarDato = true;
						_finalizarProg = true;
					}
					else
					{

						Console.WriteLine("Error. Por favor presione solo S o N");

					}

				}
			}
			

			// Mostrar resultados

			Console.WriteLine();
			Console.WriteLine($"La cantidad de numeros pares ingresados es: {_contadorPar}");
			Console.WriteLine($"La cantidad de numeros perfecto ingresados es: {_contadorPerfecto}");
			Console.WriteLine($"La cantidad de numeros primo ingresados es: {_contadorPrimo}");


			Console.WriteLine("\nPresione cualquier tecla para finalizar...");

			Console.ReadKey();

			Creditos();

			Console.ReadKey();
		}
	}
}
