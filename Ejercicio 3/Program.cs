﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio_3
{
	class Program
	{

		public static void Menu()
		{

			Console.WriteLine("---CAJERO AUTOMATICO---");
			Console.WriteLine("1- DEPOSITO");
			Console.WriteLine("2- EXTRACCION");
			Console.WriteLine("3- SALDO");
			Console.WriteLine("4- COMPRAR DOLARES");
			Console.WriteLine("0- SALIR");

		}

		public static decimal Deposito(decimal saldo)
		{
			
			decimal _deposito = 0;
			Console.Clear();
			Console.WriteLine("---DEPOSITO---");
			Console.Write("Ingrese la cantidad a depositar: ");
			if (decimal.TryParse(Console.ReadLine(), out _deposito) && _deposito >0)
			{
				saldo += _deposito;
			}
			else
			{
				Console.WriteLine("\nOcurrio un error. Por favor deposite un monto mayor a cero");
				Console.WriteLine("Presione cualquier tecla para volver atras");
				Console.ReadKey();
			}
			
			return saldo;
		}

		public static decimal Extraccion(decimal saldo)
		{
			
			decimal _extraccion = 0;
			Console.Clear();
			Console.WriteLine("---EXTRACCION---");
			Console.Write("Ingrese la cantidad de dinero a extraer: ");
			if(decimal.TryParse(Console.ReadLine(), out _extraccion) && _extraccion < saldo)
			{
				saldo -= _extraccion;
			}
			else
			{
				Console.WriteLine("\nEl saldo que usted posee es menor al que quiere extraer");
				Console.WriteLine("Presione cualquier tecla para volver atras");
			}

			return saldo;
		}
		public static void Saldo(decimal saldo)
		{
			
			Console.Clear();
			Console.WriteLine("---SALDO---");
			Console.WriteLine($"Su saldo actual es ${saldo}");
			Console.ReadKey(); 
			
		}
		public static decimal ComprarDolares(decimal saldo)
		{
			decimal _cotizacionDolar = 0; // cotizacion 
			decimal _cantidadDolares = 0; // cantidad dolares a comprar
			decimal _dolaresAdquiridos = 0; // cantidad adquirida
			decimal _compraDolar = 0; // dolares total a comprar
			
			Console.Clear();
			Console.WriteLine("---COMPRAR DOLARES---");
			
			Console.Write("Ingrese la cotizacion del dolar: ");
			if(decimal.TryParse(Console.ReadLine(), out _cotizacionDolar))
			{
				Console.Write("Ingrese la cantidad de dolares que quiere comprar: ");
				if(decimal.TryParse(Console.ReadLine(), out _cantidadDolares))
				{
					_compraDolar = _cantidadDolares * _cotizacionDolar; // Cantidad dolares
					if(_compraDolar <= saldo)
					{
						_dolaresAdquiridos = _cantidadDolares; // dolares adquiridos
						saldo -= _compraDolar; // saldo restante
						Console.WriteLine($"Usted a comprado U$D {_dolaresAdquiridos}");
					}
					else
					{
						Console.WriteLine("\nUsted no posee el dinero suficiente para comprar dolares");
					}
				}
				else
				{

				}
			}
			Console.ReadKey();

			return saldo;
		}


		static void Main(string[] args)
		{

			// Declarar variables
			decimal _saldoUsuario = 0; // saldo de lo que tiene
			int _opcionMenu = 0; // swich
			bool _verificarDato = false;
			bool _finalizarPrograma = false;

			Console.Clear();
			Menu();
			while(_finalizarPrograma == false)
			{
				_verificarDato = false;
				while(_verificarDato == false)
				{
					Console.Clear();
					Menu();
					Console.Write("\nPresione la opcion que desee ejecutar: ");
					if (int.TryParse(Console.ReadLine(), out _opcionMenu))
					{
						switch (_opcionMenu)
						{
							case 1:
								_saldoUsuario = Deposito(_saldoUsuario);
								break;
							case 2:
								_saldoUsuario = Extraccion(_saldoUsuario);
								break;
							case 3:
								Saldo(_saldoUsuario);
								break;
							case 4:
								_saldoUsuario = ComprarDolares(_saldoUsuario);
								break;
							case 0:
								_finalizarPrograma = true;
								break;
								
						}
						_verificarDato = true;
					}
					
				}
			}
			Console.WriteLine("\nGracias por su visita!");
			Console.ReadKey();
		}
	}
}
