﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio_5
{
	class Program

	{
		public static int AnalizarPunto(int a, int b)
		{
			int _res = 0;

			// Ubicacion cuadrantes
			int _primerCuadrante = 0; // x,y
			int _segundoCuadrante = 0; // -x,y
			int _tercerCuadrante = 0; // -x,-y
			int _cuartoCuadrante = 0; // x,-y
			int _ejeCentral = 0; // 0;0
			int _contadorGeneral = 0; // contador de puntos

			// Operacion

			if(a >= 0 && b >= 0) // eje central; finalizacion
			{
				_ejeCentral++;
				_contadorGeneral++;
			}
			else if (a > 0 && b > 0) // primer cuadrante
			{
				_contadorGeneral++;
				_primerCuadrante++;
				Console.WriteLine("El punto se encuentra en el primer cuadrante");
			}
			else if (a < 0 && b > 0) // segundo cuadrante
			{
				_contadorGeneral++;
				_segundoCuadrante++;
			}
			else if (a < 0 && b < 0) // tercer cuadrante
			{
				_tercerCuadrante++;
				_contadorGeneral++;
			}
			else if (a > 0 && b < 0) // cuarto cuadrante
			{
				_cuartoCuadrante++;
				_contadorGeneral++;
			}


			return _res;
		}


		static void Main(string[] args)
		{

			//Variables

			int _x = 0;
			int _y = 0;
			int _ubicacion = 0;

			/*Ubicacion cuadrantes
			int _primerCuadrante = 0; // x,y
			int _segundoCuadrante = 0; // -x,y
			int _tercerCuadrante = 0; // -x,-y
			int _cuartoCuadrante = 0; // x,-y
			*/


			// Operar

			Console.WriteLine("Ingrese la coordenada X: ");
			if (int.TryParse(Console.ReadLine(), out _x))
			{
				Console.WriteLine("Ingrese la coordenada Y: ");
				if(int.TryParse(Console.ReadLine(), out _y))
				{
					_ubicacion = AnalizarPunto(_x, _y);
					Console.WriteLine($"El punto se encuentra en {_ubicacion}");
					

				}
			}
			Console.ReadKey();
		}
	}
}
