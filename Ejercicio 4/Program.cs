﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio_4
{
	class Program
	{
		// a = x1
		// b = x2
		// c = y1
		// d = y2


		public static double DistanciaEntre (double a, double b , double c , double d) 
		{
			double _resultado = 0;


			_resultado = Math.Sqrt(Math.Pow(b - a, 2) + Math.Pow(d - c, 2));

			return _resultado;
		}
		static void Main(string[] args)
		{

			// declarar variables
			double _distancia = 0;
			double _x1 = 0;
			double _x2 = 0;
			double _y1 = 0;
			double _y2 = 0;

			Console.WriteLine("Ingrese el valor de x1: ");
			if (double.TryParse(Console.ReadLine(), out _x1))
			{
				Console.WriteLine("Ingrese el valor de x2: ");
				if (double.TryParse(Console.ReadLine(), out _x2))
				{
					Console.WriteLine("Ingrese el valor de y1: ");
					if (double.TryParse(Console.ReadLine(), out _y1))
					{
						Console.WriteLine("Ingrese el valor de y2: ");
						if (double.TryParse(Console.ReadLine(), out _y2))
						{
							_distancia = DistanciaEntre(_x1, _x2, _y1, _y2);
						}
					}
				}
			}
			Console.WriteLine($"La distancia entre X e Y es de {Decimal.Round((Decimal)_distancia, 2)} ");
			Console.ReadKey();
		}
	}
}
